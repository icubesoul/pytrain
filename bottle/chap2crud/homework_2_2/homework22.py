import pymongo
import sys
import json

#set database connection
connection = pymongo.MongoClient('localhost',27017)
db = connection.students
grades = db.grades
#drop grades collection
grades.drop()

#import grades collection from json to the database
#parse the json into python objects
with open("grades.json") as json_data:
    for row in json_data:
        data = json.loads(row)
        data.pop('_id')
        grades.insert_one(data)

#iterate through grades and remove document for homework with lowest grade
query={'type':'homework'}
cursor = grades.find(query).sort([('student_id',pymongo.ASCENDING),('score',pymongo.ASCENDING)])
studentid=-1
for doc in cursor:
    if studentid <> doc['student_id']:
        studentid = doc['student_id']
        result = grades.delete_one({'_id':doc['_id']})
        


